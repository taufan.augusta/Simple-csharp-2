﻿using System;

namespace ConsoleApp2
{
    class Program
    {
        static void Main(string[] args)
        {
            float weight, height;
            IdealMass mass = new IdealMass();
            
            Console.Write("Weight: ");
            weight = float.Parse(Console.ReadLine());
            Console.Write("Height: ");
            height = float.Parse(Console.ReadLine());

            string result = mass.Category(mass.Ideal(weight, height));
            Console.WriteLine($"Weigth : Height ratio - {weight} : {height} is {result}");
        
        }
    }
}
