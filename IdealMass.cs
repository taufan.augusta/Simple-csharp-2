namespace ConsoleApp2
{
    public class IdealMass
    {
        public float Ideal(float weight, float heigth)
            => weight / heigth;
    
        public string Category(float mass)
        {
            if(mass < 14.9f)
                return "Malnutrition";
            else if(mass > 15 && mass < 18.4f)
                return "Thin";
            else if(mass > 18.5f && mass < 22.9f)
                return "Ideal";
            else if(mass > 27.6 && mass < 40f)
                return "Fat";
            else
                return "Obesity";
        }
    
    }
}